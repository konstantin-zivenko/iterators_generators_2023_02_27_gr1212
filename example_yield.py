def infinite_sequence(stop):
  num = 0
  while True:
    yield num
    num += 1
    if num == stop:
        raise StopIteration

for i in infinite_sequence(50):
    print(i)